from django.shortcuts import render

from .models import BookProgramLang


def book_html(request):
    book = BookProgramLang.objects.all()
    return render(request,
                  'book.html', {'book': book})
