from django.db import models


class BookProgramLang(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='book_images/')
    description = models.TextField()
    cost = models.DecimalField(max_digits=6, decimal_places=0,null=True)
    color = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

