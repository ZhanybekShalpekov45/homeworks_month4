from django.urls import path

from . import views

urlpatterns = [
    path('book_programm_lang/', views.book_html, name='book'),
]