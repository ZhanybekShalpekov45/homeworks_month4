from django.apps import AppConfig


class tv_show(AppConfig):
    default_auto_field = 'django.db.models.tv_show'
    name = 'tv_show'
