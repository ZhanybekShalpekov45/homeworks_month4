from django.shortcuts import render

from . import models


def tv_show_view(request):
    tv_show = models.TvShow.objects.all()
    return render(request, 'tv_show/tv_show.html', {'tv_show': tv_show})

