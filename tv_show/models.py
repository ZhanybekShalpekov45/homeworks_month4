from django.db import models


class TvShow(models.Model):
    TYPE_TV_SHOW = (
        ('Комедийные', 'Комедийные'),
        ('Научные', 'Научные'),
        ('Новости', 'Новости'),
        ('Хроника', 'Хроника')
    )
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='tv_show/')
    description = models.TextField()
    type_tv_show = models.CharField(max_length=100, choices=TYPE_TV_SHOW)
    url_tv_show = models.URLField()
    company_tv = models.CharField(max_length=35)
    age_restrictions = models.IntegerField()
    number_tv_show = models.IntegerField()
    instagram = models.URLField()
    about_tv_show = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
